#!/bin/bash

buildah bud --no-cache -t learn-k3s-app:linux-arm64 -f Containerfile --build-arg ARCH=linux/arm64 --build-arg HTTP_PORT=80 --build-arg DOMAIN=berde.bar .
buildah tag localhost/learn-k3s-app:linux-arm64 localhost:5000/learn-k3s-app:linux-arm64
buildah push localhost:5000/learn-k3s-app:linux-arm64
